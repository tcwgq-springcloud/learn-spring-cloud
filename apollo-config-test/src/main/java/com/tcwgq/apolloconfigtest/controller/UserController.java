package com.tcwgq.apolloconfigtest.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tcwgq
 * @since 2023/4/19 21:25
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Value("${person.name}")
    private String username;

    @GetMapping("/name")
    public String name() {
        return username;
    }

}
