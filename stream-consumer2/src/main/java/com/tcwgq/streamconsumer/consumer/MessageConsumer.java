package com.tcwgq.streamconsumer.consumer;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @author tcwgq
 * @since 2023/4/18 20:33
 */
@Component
@EnableBinding(Sink.class)
public class MessageConsumer {
    // 监听 binding 为 Sink.INPUT 的消息
    @StreamListener(Sink.INPUT)
    public void input(Message<String> message){
        System.out.println("监听收到：" + message.getPayload());
    }

}
