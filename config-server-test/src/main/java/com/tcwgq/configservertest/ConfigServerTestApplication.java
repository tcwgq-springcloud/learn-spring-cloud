package com.tcwgq.configservertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigServerTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigServerTestApplication.class, args);
    }

}
