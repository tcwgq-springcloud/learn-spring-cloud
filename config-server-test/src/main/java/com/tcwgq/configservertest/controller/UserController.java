package com.tcwgq.configservertest.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tcwgq
 * @since 2023/4/19 21:25
 */
@RefreshScope //开启动态刷新
@RestController
@RequestMapping("/user")
public class UserController {
    @Value("${person.name}")
    private String username;

    @GetMapping("/name")
    public String name() {
        return username;
    }

}
