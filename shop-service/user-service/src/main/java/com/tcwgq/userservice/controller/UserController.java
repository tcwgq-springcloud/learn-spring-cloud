package com.tcwgq.userservice.controller;

import com.tcwgq.userservice.entity.User;
import com.tcwgq.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Value("${server.port}")
    private String port;

    @Value("${spring.cloud.client.ip-address}") // spring cloud 自动的获取当前应用的ip地址
    private String ip;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User findById(@PathVariable Long id) {
        User user = userService.findById(id);
        user.setAddress("访问的服务地址:" + ip + ":" + port);
        return user;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String save(@RequestBody User user) {
        userService.save(user);
        return "保存成功";
    }
}
