package com.tcwgq.userservice.service;

import com.tcwgq.userservice.entity.User;

public interface UserService {

    /**
     * 根据id查询
     */
    User findById(Long id);

    /**
     * 保存
     */
    void save(User user);

    /**
     * 更新
     */
    void update(User user);

    /**
     * 删除
     */
    void delete(Long id);
}
