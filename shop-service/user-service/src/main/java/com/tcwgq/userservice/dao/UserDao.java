package com.tcwgq.userservice.dao;

import com.tcwgq.userservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 接口继承
 */
public interface UserDao extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

}
