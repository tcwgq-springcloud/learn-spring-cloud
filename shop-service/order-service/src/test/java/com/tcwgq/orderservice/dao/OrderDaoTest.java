package com.tcwgq.orderservice.dao;

import com.tcwgq.orderservice.entity.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

/**
 * @author tcwgq
 * @since 2023/3/23 21:47
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class OrderDaoTest {
    @Autowired
    private OrderDao orderDao;

    @Test
    public void findById() {
        Optional<Order> optional = orderDao.findById(1L);
        System.out.println(optional.get());
    }

}