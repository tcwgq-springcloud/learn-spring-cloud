package com.tcwgq.orderservice.service.impl;

import com.tcwgq.orderservice.dao.OrderDao;
import com.tcwgq.orderservice.entity.Order;
import com.tcwgq.orderservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDao orderDao;

	@Override
	public Order findById(Long id) {
		return orderDao.findById(id).get();
	}

	@Override
	public void save(Order order) {
		orderDao.save(order);
	}

	@Override
	public void update(Order order) {
		orderDao.save(order);
	}

	@Override
	public void delete(Long id) {
		orderDao.deleteById(id);
	}
}
