package com.tcwgq.orderservice.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author tcwgq
 * @since 2023/3/23 20:50
 */
@Data
@Entity
@Table(name="tb_order")
public class Order {
    @Id
    private Long id;
    private Long userId;
    private Long productId;
    private Integer number;
    private BigDecimal price;
    private BigDecimal amount;
    private String productName;
    private String username;

}
