package com.tcwgq.orderservice.service;

import com.tcwgq.orderservice.entity.Order;

public interface OrderService {

    /**
     * 根据id查询
     */
    Order findById(Long id);

    /**
     * 保存
     */
    void save(Order order);

    /**
     * 更新
     */
    void update(Order order);

    /**
     * 删除
     */
    void delete(Long id);
}
