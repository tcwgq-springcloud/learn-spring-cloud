package com.tcwgq.orderservice.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.tcwgq.productapi.feign.ProductFeign;
import com.tcwgq.productapi.model.ProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/order")
/**
 * @DefaultProperties : 指定此接口中公共的熔断设置
 *      如果过在@DefaultProperties指定了公共的降级方法
 *      在@HystrixCommand不需要单独指定了
 */
// @DefaultProperties(defaultFallback = "defaultFallBack")
public class OrderController {

	//注入restTemplate对象
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * 注入DiscoveryClient :
	 *  springcloud提供的获取原数组的工具类
	 *      调用方法获取服务的元数据信息
	 *
	 */
	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	private ProductFeign productFeign;


	/**
	 * 基于ribbon的形式调用远程微服务
	 *  1.使用@LoadBalanced声明RestTemplate
	 *  2.使用服务名称替换ip地址
	 */
	/**
	 * 使用注解配置熔断保护
	 *     fallbackmethod : 配置熔断之后的降级方法
	 */
	@HystrixCommand(fallbackMethod = "findByIdFallback")
	// @HystrixCommand // 使用统一降级方法
	@RequestMapping(value = "/buy/{id}",method = RequestMethod.GET)
	public ProductVO findById(@PathVariable Long id) {
		// ribbon通过服务名进行调用，实现客户端负载均衡
		// 使用restTemplate测试hystrix熔断状态
		if(id != 1){
			throw new RuntimeException("我异常了");
		}
		return restTemplate.getForObject("http://service-product/product/" + id, ProductVO.class); //restTemplate调用
		// return productFeign.findById(id);// feign调用
		// return new OrderCommand(restTemplate, id).execute();// 熔断调用
	}

	/**
	 * 指定统一的降级方法
	 *  * 参数 : 没有参数
	 */
	public ProductVO defaultFallBack() {
		ProductVO product = new ProductVO();
		product.setProductName("触发统一的降级方法");
		return product;
	}

	/**
	 * 降级方法
	 *  和需要收到保护的方法的返回值一致
	 *  方法参数一致
	 */
	public ProductVO findByIdFallback(Long id) {
		ProductVO product = new ProductVO();
		product.setProductName("不好意思,出错了");
		return product;
	}

	/**
	 * 参数:商品id
	 *  通过订单系统,调用商品服务根据id查询商品信息
	 *      1.需要配置商品对象
	 *      2.需要调用商品服务
	 *  使用java中的urlconnection,httpclient,okhttp
	 */
//	@RequestMapping(value = "/buy/{id}",method = RequestMethod.GET)
//	public Product findById(@PathVariable Long id) {
//		//调用discoveryClient方法
//		//已调用服务名称获取所有的元数据
//		List<ServiceInstance> instances = discoveryClient.getInstances("service-product");
//		//获取唯一的一个元数据
//		ServiceInstance instance = instances.get(0);
//		//根据元数据中的主机地址和端口号拼接请求微服务的URL
//		Product product = null;
//		//如何调用商品服务?
//		product = restTemplate.getForObject("http://"+instance.getHost()+":"+instance.getPort()+"/product/1",Product.class);
//		return product;
//	}


}
