package com.tcwgq.orderservice.dao;

import com.tcwgq.orderservice.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 接口继承
 */
public interface OrderDao extends JpaRepository<Order,Long>, JpaSpecificationExecutor<Order> {

}
