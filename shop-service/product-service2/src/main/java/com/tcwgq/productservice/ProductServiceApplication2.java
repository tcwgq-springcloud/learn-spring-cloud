package com.tcwgq.productservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

// 激活eurekaClient
@EnableEurekaClient
//@EnableDiscoveryClient
@SpringBootApplication
@EntityScan("com.tcwgq.productservice.entity")
public class ProductServiceApplication2 {

    public static void main(String[] args) {
        SpringApplication.run(ProductServiceApplication2.class, args);
    }

}
