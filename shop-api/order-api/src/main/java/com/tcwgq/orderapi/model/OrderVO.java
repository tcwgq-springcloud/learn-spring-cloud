package com.tcwgq.orderapi.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author tcwgq
 * @since 2023/3/23 20:50
 */
@Data
public class OrderVO {
    private Long id;
    private Long userId;
    private Long productId;
    private Integer number;
    private BigDecimal price;
    private BigDecimal amount;
    private String productName;
    private String username;

}
