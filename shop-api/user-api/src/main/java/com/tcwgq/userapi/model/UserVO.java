package com.tcwgq.userapi.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 用户实体类
 */
@Data
public class UserVO {

	private Long id;
	private String username;
	private String password;
	private Integer age;
	private BigDecimal balance;
	private String address;
}
