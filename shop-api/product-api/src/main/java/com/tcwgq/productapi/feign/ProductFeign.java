package com.tcwgq.productapi.feign;

import com.tcwgq.productapi.falllback.ProductFeignFallback;
import com.tcwgq.productapi.falllback.ProductFeignFallbackFactory;
import com.tcwgq.productapi.model.ProductVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 声明需要调用的微服务名称
 *  @FeignClient
 *      * name : 服务提供者的名称
 *      * fallback : 配置熔断发生降级方法
 *                  实现类
 */
@FeignClient(name = "service-product", /* fallback = ProductFeignFallback.class */ fallbackFactory = ProductFeignFallbackFactory.class)
// @RestController // 不能加这个注解，否则报Ambiguous mapping
// @RequestMapping("/product")
public interface ProductFeign {
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    ProductVO findById(@PathVariable Long id);

}
