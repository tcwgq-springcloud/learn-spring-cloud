package com.tcwgq.productapi.falllback;

import com.tcwgq.productapi.feign.ProductFeign;
import com.tcwgq.productapi.model.ProductVO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author tcwgq
 * @since 2023/3/26 10:19
 */
@Slf4j
@Component
public class ProductFeignFallbackFactory implements FallbackFactory<ProductFeign> {
    @Override
    public ProductFeign create(Throwable cause) {
        return id -> {
            log.error("触发统一的ProductFeign降级方法，message={}", cause.getMessage(), cause);
            ProductVO productVO = new ProductVO();
            productVO.setProductName("触发统一的ProductFeign降级方法");
            return productVO;
        };
    }

}
