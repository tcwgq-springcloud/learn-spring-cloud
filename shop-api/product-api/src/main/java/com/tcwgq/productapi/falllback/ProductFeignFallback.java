package com.tcwgq.productapi.falllback;

import com.tcwgq.productapi.feign.ProductFeign;
import com.tcwgq.productapi.model.ProductVO;
import org.springframework.stereotype.Component;

/**
 * @author tcwgq
 * @since 2023/3/26 10:19
 */
@Component
public class ProductFeignFallback implements ProductFeign {
    @Override
    public ProductVO findById(Long id) {
        ProductVO productVO = new ProductVO();
        productVO.setProductName("触发统一的ProductFeign降级方法");
        return productVO;
    }

}
