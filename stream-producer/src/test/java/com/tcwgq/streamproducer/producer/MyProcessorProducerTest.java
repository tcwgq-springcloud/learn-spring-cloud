package com.tcwgq.streamproducer.producer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author tcwgq
 * @since 2023/4/18 20:58
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MyProcessorProducerTest {
    @Autowired
    private MyProcessorProducer myProcessorProducer;

    @Test
    public void send() {
        myProcessorProducer.send("Hello world, java!");
    }

    @Test
    public void send1() {
        for (int i = 0; i < 5; i++) {
            myProcessorProducer.send("1");
        }
    }

}