package com.tcwgq.streamproducer.producer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author tcwgq
 * @since 2023/4/18 20:35
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageProducerTest {
    @Autowired
    private MessageProducer messageProducer;

    @Test
    public void send() {
        messageProducer.send("Hello world");
    }

}