package com.tcwgq.streamconsumer.consumer;

import com.tcwgq.streamconsumer.channel.MyProcessor;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @author tcwgq
 * @since 2023/4/18 20:33
 */
@Component
@EnableBinding(MyProcessor.class)
public class MyProcessorConsumer {
    // 监听 binding 为 Sink.INPUT 的消息
    @StreamListener(MyProcessor.MYINPUT)
    public void input(Message<String> message){
        System.out.println("MyProcessorConsumer监听收到：" + message.getPayload());
    }

}
